import unittest
import json
import sys


class Part:
    def __init__(self, typename, index, array):
        if len(array) != 9 or typename not in Part.types():
            raise ValueError
        self.__index = index
        self.__array = array
        self.__typename = typename

    def __getitem__(self, key):
        return self.__array[key]

    def __len__(self):
        return self.__array.__len__()

    def __eq__(self, other):
        return self.__typename == other.__typename \
            and self.__array == other.__array \
            and self.__index == other.__index

    def type(self):
        return self.__typename

    def is_row(self):
        return self.__typename == "row"

    def is_column(self):
        return self.__typename == "column"

    def is_square(self):
        return self.__typename == "square"

    def index(self):
        return self.__index

    @staticmethod
    def complements(parttype):
        if parttype not in Part.types():
            raise ValueError
        return list(filter(lambda e: e != parttype, Part.types()))

    @staticmethod
    def types():
        return ["row", "column", "square"]


class Field:
    def __init__(self, field):
        if len(field) != 9 * 9:
            raise ValueError
        self.__field = field

    def __column(self, n):
        return Part("column", n, [self.__field[i * 9 + n] for i in range(9)])

    def __column_from_row(self, row, offset):
        return (self.part("column", offset), row)

    def __column_from_square(self, square, offset):
        row, roffset = self.__row_from_square(square, offset)
        return self.__column_from_row(row.index(), roffset)

    def __column_from(self, part, offset):
        if part.type() == "square":
            return self.__column_from_square(part.index(), offset)
        elif part.type() == "row":
            return self.__column_from_row(part.index(), offset)
        elif part.type() == "column":
            return (self.part(part.type(), part.index()), offset)
        else:
            raise ValueError

    def __row(self, n):
        return Part("row", n, self.__field[n * 9:n * 9 + 9])

    def __row_from_column(self, column, offset):
        return (self.part("row", offset), column)

    def __row_from_square(self, square, offset):
        row = offset // 3 + square // 3 * 3
        roffset = offset % 3 + square % 3 * 3
        return (self.part("row", row), roffset)

    def __row_from(self, part, offset):
        if part.type() == "square":
            return self.__row_from_square(part.index(), offset)
        elif part.type() == "row":
            return (self.part(part.type(), part.index()), offset)
        elif part.type() == "column":
            return self.__row_from_column(part.index(), offset)
        else:
            raise ValueError

    def __square(self, n):
        first = (n % 3) * 3 + (n // 3) * 9 * 3
        second = (n % 3) * 3 + (n // 3) * 9 * 3 + 9
        third = (n % 3) * 3 + (n // 3) * 9 * 3 + 18
        return Part("square", n, self.__field[first:first + 3] +
                    self.__field[second:second + 3] +
                    self.__field[third:third + 3])

    def __square_from_row(self, row, offset):
        square = offset // 3 + row // 3 * 3
        soffset = offset % 3 + row % 3 * 3
        return (self.part("square", square), soffset)

    def __square_from_column(self, col, offset):
        row, roffset = self.__row_from_column(col, offset)
        return self.__square_from_row(row.index(), roffset)

    def __square_from(self, part, offset):
        if part.type() == "square":
            return (self.part(part.type(), part.index()), offset)
        elif part.type() == "row":
            return self.__square_from_row(part.index(), offset)
        elif part.type() == "column":
            return self.__square_from_column(part.index(), offset)
        else:
            raise ValueError

    def part(self, parttype, n):
        if parttype == "square":
            return self.__square(n)
        elif parttype == "row":
            return self.__row(n)
        elif parttype == "column":
            return self.__column(n)
        else:
            raise ValueError

    def parts(self, parttype):
        return [self.part(parttype, i) for i in range(9)]

    def part_from(self, parttype, other_part, offset):
        if parttype == "square":
            return self.__square_from(other_part, offset)
        elif parttype == "row":
            return self.__row_from(other_part, offset)
        elif parttype == "column":
            return self.__column_from(other_part, offset)
        else:
            raise ValueError

    def set_at(self, part, index, number):
        if part.type() == "square":
            row, column_index = self.__row_from_square(part.index(), index)
            self.__field[row.index() * 9 + column_index] = number
        elif part.type() == "row":
            self.__field[part.index() * 9 + index] = number
        elif part.type() == "column":
            self.__field[index * 9 + part.index()] = number
        else:
            raise ValueError

    def __str__(self):
        triples = [' '.join(list(map(str, self.__field))[i * 3:i * 3 + 3])
                   for i in range(3 * 9)]
        rows = ['  '.join(triples[i * 3:i * 3 + 3]) for i in range(9)]
        trirows = ['\n'.join(rows[i * 3:i * 3 + 3]) for i in range(3)]
        complete_field = '\n\n'.join(trirows)
        return complete_field


class Logic:
    @staticmethod
    def _count_of(part, number):
        return sum(map(lambda n: int(n == number), part))

    @staticmethod
    def count_of(part, number):
        if number < 1 or number > 9:
            raise ValueError
        return Logic._count_of(part, number)

    @staticmethod
    def count_of_empty(part):
        return Logic._count_of(part, 0)

    @staticmethod
    def places_in_part(field, part, number):
        complements = Part.complements(part.type())
        places = []
        if Logic.count_of(part, number) == 0:
            for i, n in enumerate(part):
                if n == 0:
                    complement0, _ = field.part_from(complements[0], part, i)
                    complement1, _ = field.part_from(complements[1], part, i)
                    if Logic.count_of(complement0, number) > 0:
                        continue
                    if Logic.count_of(complement1, number) > 0:
                        continue
                    places.append(i)
        return places


class Validation:
    @staticmethod
    def _are_parts_valid(parts):
        for row in parts:
            arr = [0, 0, 0, 0, 0, 0, 0, 0, 0]
            for n in row:
                if n >= 1 and n <= 9:
                    arr[n - 1] += 1
                elif n != 0:
                    return False
            arr.sort(reverse=True)
            if arr[0] > 1:
                return False
        return True

    @staticmethod
    def are_rows_valid(field):
        return Validation._are_parts_valid(field.parts("row"))

    @staticmethod
    def are_columns_valid(field):
        return Validation._are_parts_valid(field.parts("column"))

    @staticmethod
    def are_squares_valid(field):
        return Validation._are_parts_valid(field.parts("square"))

    @staticmethod
    def is_valid_sudoku(field):
        return Validation.are_rows_valid(field) and \
            Validation.are_columns_valid(field) and \
            Validation.are_squares_valid(field)

    @staticmethod
    def is_complete_sudoku(field):
        for row in field.parts("row"):
            if Logic.count_of_empty(row) > 0:
                return False
        return True


class TestPart(unittest.TestCase):
    def test_part_is_accessible_by_index(self):
        p = Part("row", 0, [x for x in range(9)])
        self.assertEqual(p[1], 1)

    def test_part_is_iterable(self):
        p = Part("row", 0, [x for x in range(9)])
        for e in p:
            self.assertEqual(type(e), type(1))

    def test_part_has_len(self):
        p = Part("row", 0, [x for x in range(9)])
        self.assertEqual(len(p), 9)

    def test_part_has_type_info_of_being_row_column_or_square(self):
        p = Part("row", 0, [x for x in range(9)])
        self.assertEqual(p.type(), "row")
        self.assertTrue(p.is_row())
        self.assertFalse(p.is_column())
        self.assertFalse(p.is_square())
        p = Part("column", 0, [x for x in range(9)])
        self.assertEqual(p.type(), "column")
        self.assertTrue(p.is_column())
        self.assertFalse(p.is_row())
        self.assertFalse(p.is_square())
        p = Part("square", 0, [x for x in range(9)])
        self.assertEqual(p.type(), "square")
        self.assertTrue(p.is_square())
        self.assertFalse(p.is_column())
        self.assertFalse(p.is_row())

    def test_part_raises_value_error_if_typename_is_invalid(self):
        with self.assertRaises(ValueError):
            Part("raw", 0, [x for x in range(9)])

    def test_part_raises_value_error_if_array_length_is_not_9(self):
        with self.assertRaises(ValueError):
            Part("row", 0, [x for x in range(10)])
        with self.assertRaises(ValueError):
            Part("row", 0, [x for x in range(8)])

    def test_two_identical_by_content_columns_are_equal(self):
        self.assertEqual(
            Part("column", 0, [x for x in range(9)]),
            Part("column", 0, [x for x in range(9)]))

    def test_part_has_index_property(self):
        p = Part("row", 1, [x for x in range(9)])
        self.assertEqual(p.index(), 1)

    def test_part_raises_value_error_if_index_out_of_range(self):
        with self.assertRaises(ValueError):
            Part("row", -1, [x for x in range(10)])
        with self.assertRaises(ValueError):
            Part("row", 9, [x for x in range(10)])
        with self.assertRaises(ValueError):
            Part("row", 100, [x for x in range(10)])


class TestSudokuCommonData(unittest.TestCase):
    field_columns = [
        1, 2, 3, 4, 5, 6, 7, 8, 9,
        1, 2, 3, 4, 5, 6, 7, 8, 9,
        1, 2, 3, 4, 5, 6, 7, 8, 9,

        1, 2, 3, 4, 5, 6, 7, 8, 9,
        1, 2, 3, 4, 5, 6, 7, 8, 9,
        1, 2, 3, 4, 5, 6, 7, 8, 9,

        1, 2, 3, 4, 5, 6, 7, 8, 9,
        1, 2, 3, 4, 5, 6, 7, 8, 9,
        1, 2, 3, 4, 5, 6, 7, 8, 9,
    ]
    field_rows = [
        1, 1, 1, 1, 1, 1, 1, 1, 1,
        2, 2, 2, 2, 2, 2, 2, 2, 2,
        3, 3, 3, 3, 3, 3, 3, 3, 3,

        4, 4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5, 5,
        6, 6, 6, 6, 6, 6, 6, 6, 6,

        7, 7, 7, 7, 7, 7, 7, 7, 7,
        8, 8, 8, 8, 8, 8, 8, 8, 8,
        9, 9, 9, 9, 9, 9, 9, 9, 9,
    ]
    field_squares = [
        1, 1, 1, 2, 2, 2, 3, 3, 3,
        1, 1, 1, 2, 2, 2, 3, 3, 3,
        1, 1, 1, 2, 2, 2, 3, 3, 3,

        4, 4, 4, 5, 5, 5, 6, 6, 6,
        4, 4, 4, 5, 5, 5, 6, 6, 6,
        4, 4, 4, 5, 5, 5, 6, 6, 6,

        7, 7, 7, 8, 8, 8, 9, 9, 9,
        7, 7, 7, 8, 8, 8, 9, 9, 9,
        7, 7, 7, 8, 8, 8, 9, 9, 9,
    ]
    field_squares_correct = [
        1, 2, 3, 1, 2, 3, 1, 2, 3,
        4, 5, 6, 4, 5, 6, 4, 5, 6,
        7, 8, 9, 7, 8, 9, 7, 8, 9,

        1, 2, 3, 1, 2, 3, 1, 2, 3,
        4, 5, 6, 4, 5, 6, 4, 5, 6,
        7, 8, 9, 7, 8, 9, 7, 8, 9,

        1, 2, 3, 1, 2, 3, 1, 2, 3,
        4, 5, 6, 4, 5, 6, 4, 5, 6,
        7, 8, 9, 7, 8, 9, 7, 8, 9,
    ]
    field_shifted = [
        1, 2, 3, 4, 5, 6, 7, 8, 9,
        2, 3, 4, 5, 6, 7, 8, 9, 1,
        3, 4, 5, 6, 7, 8, 9, 1, 2,

        4, 5, 6, 7, 8, 9, 1, 2, 3,
        5, 6, 7, 8, 9, 1, 2, 3, 4,
        6, 7, 8, 9, 1, 2, 3, 4, 5,

        7, 8, 9, 1, 2, 3, 4, 5, 6,
        8, 9, 1, 2, 3, 4, 5, 6, 7,
        9, 1, 2, 3, 4, 5, 6, 7, 8,
    ]
    field_shifted_str = \
        "1 2 3  4 5 6  7 8 9\n" + \
        "2 3 4  5 6 7  8 9 1\n" + \
        "3 4 5  6 7 8  9 1 2\n\n" + \
        "4 5 6  7 8 9  1 2 3\n" + \
        "5 6 7  8 9 1  2 3 4\n" + \
        "6 7 8  9 1 2  3 4 5\n\n" + \
        "7 8 9  1 2 3  4 5 6\n" + \
        "8 9 1  2 3 4  5 6 7\n" + \
        "9 1 2  3 4 5  6 7 8"
    field_correct_incomplete = [
        0, 0, 0, 2, 6, 0, 7, 0, 1,
        6, 8, 0, 0, 7, 0, 0, 9, 0,
        1, 9, 0, 0, 0, 4, 5, 0, 0,

        8, 2, 0, 1, 0, 0, 0, 4, 0,
        0, 0, 4, 6, 0, 2, 9, 0, 0,
        0, 5, 0, 0, 0, 3, 0, 2, 8,

        0, 0, 9, 3, 0, 0, 0, 7, 4,
        0, 4, 0, 0, 5, 0, 0, 3, 6,
        7, 0, 3, 0, 1, 8, 0, 0, 0,
    ]
    field_correct = [
        4, 3, 5, 2, 6, 9, 7, 8, 1,
        6, 8, 2, 5, 7, 1, 4, 9, 3,
        1, 9, 7, 8, 3, 4, 5, 6, 2,

        8, 2, 6, 1, 9, 5, 3, 4, 7,
        3, 7, 4, 6, 8, 2, 9, 1, 5,
        9, 5, 1, 7, 4, 3, 6, 2, 8,

        5, 1, 9, 3, 2, 6, 8, 7, 4,
        2, 4, 8, 9, 5, 7, 1, 3, 6,
        7, 6, 3, 4, 1, 8, 2, 5, 9,
    ]
    field_solve_square_1 = [
        0, 0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 1, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0,
    ]
    field_solve_sqare_2 = [
        1, 2, 3, 0, 0, 0, 0, 0, 0,
        5, 6, 0, 0, 0, 0, 0, 0, 0,
        7, 8, 9, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ]
    field_solve_sqare_3 = [
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 2, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 7, 8, 4, 0, 0, 0,
        0, 0, 0, 0, 5, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 2, 0,
    ]
    field_solve_row_1 = [
        1, 6, 2, 9, 4, 5, 7, 8, 0,
        0, 0, 0, 0, 0, 3, 0, 0, 0,
        3, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 3, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 3, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ]
    field_solve_row_2 = [
        1, 0, 2, 3, 4, 5, 7, 8, 9,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0,
    ]
    field_solve_column_1 = [
        0, 0, 0, 0, 9, 0, 0, 0, 0,
        9, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 3, 0, 0, 0, 0, 0,

        0, 0, 0, 4, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 9, 0,
        0, 0, 0, 6, 0, 0, 0, 0, 0,

        0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 7, 0, 0, 0, 0, 0,
        0, 0, 0, 8, 0, 0, 0, 0, 0,
    ]
    field_empty = [0 for i in range(9 * 9)]


class TestSudokuFieldMethods(TestSudokuCommonData):
    def test_invalid_argument_to_constructor_raises(self):
        with self.assertRaises(ValueError):
            Field([])

    def test_columns(self):
        f = Field(self.field_columns)
        self.assertEqual(
            f.parts("column")[0], Part("column", 0, [1 for _ in range(9)]))
        self.assertEqual(
            f.parts("column")[4], Part("column", 4, [5 for _ in range(9)]))

    def test_part_raises_value_error_if_unknown_partname_passed(self):
        f = Field(self.field_columns)
        with self.assertRaises(ValueError):
            f.part("asdf", 0)

    def test_column(self):
        f = Field(self.field_columns)
        self.assertEqual(
            f.part("column", 0), Part("column", 0, [1 for _ in range(9)]))
        self.assertEqual(
            f.part("column", 4), Part("column", 4, [5 for _ in range(9)]))

    def test_rows(self):
        f = Field(self.field_rows)
        self.assertEqual(
            f.parts("row")[8], Part("row", 8, [9 for _ in range(9)]))
        self.assertEqual(
            f.parts("row")[4], Part("row", 4, [5 for _ in range(9)]))
        self.assertEqual(
            f.parts("row")[3], Part("row", 3, [4 for _ in range(9)]))

    def test_row(self):
        f = Field(self.field_rows)
        self.assertEqual(
            f.part("row", 0), Part("row", 0, [1 for _ in range(9)]))
        self.assertEqual(
            f.part("row", 5), Part("row", 5, [6 for _ in range(9)]))
        self.assertEqual(
            f.part("row", 8), Part("row", 8, [9 for _ in range(9)]))

    def test_square(self):
        f = Field(self.field_squares)
        self.assertEqual(
            f.part("square", 0), Part("square", 0, [1 for _ in range(9)]))
        self.assertEqual(
            f.part("square", 3), Part("square", 3, [4 for _ in range(9)]))
        self.assertEqual(
            f.part("square", 7), Part("square", 7, [8 for _ in range(9)]))

    def test_str(self):
        self.assertEqual(str(Field(self.field_shifted)),
                         self.field_shifted_str)

    def test_row_from_culumn(self):
        f = Field(self.field_shifted)
        self.assertEqual(
            f.part_from("row", f.part("column", 0), 0), (f.part("row", 0), 0))
        self.assertEqual(
            f.part_from("row", f.part("column", 1), 0), (f.part("row", 0), 1))
        self.assertEqual(
            f.part_from("row", f.part("column", 3), 0), (f.part("row", 0), 3))
        self.assertEqual(
            f.part_from("row", f.part("column", 4), 4), (f.part("row", 4), 4))
        self.assertEqual(
            f.part_from("row", f.part("column", 1), 8), (f.part("row", 8), 1))
        self.assertEqual(
            f.part_from("row", f.part("column", 8), 8), (f.part("row", 8), 8))

    def test_row_from_square(self):
        f = Field(self.field_shifted)
        self.assertEqual(
            f.part_from("row", f.part("square", 0), 0), (f.part("row", 0), 0))
        self.assertEqual(
            f.part_from("row", f.part("square", 0), 6), (f.part("row", 2), 0))
        self.assertEqual(
            f.part_from("row", f.part("square", 0), 8), (f.part("row", 2), 2))
        self.assertEqual(
            f.part_from("row", f.part("square", 1), 0), (f.part("row", 0), 3))
        self.assertEqual(
            f.part_from("row", f.part("square", 3), 0), (f.part("row", 3), 0))
        self.assertEqual(
            f.part_from("row", f.part("square", 4), 4), (f.part("row", 4), 4))
        self.assertEqual(
            f.part_from("row", f.part("square", 1), 8), (f.part("row", 2), 5))
        self.assertEqual(
            f.part_from("row", f.part("square", 8), 8), (f.part("row", 8), 8))
        self.assertEqual(
            f.part_from("row", f.part("square", 5), 5), (f.part("row", 4), 8))
        self.assertEqual(
            f.part_from("row", f.part("square", 6), 6), (f.part("row", 8), 0))

    def test_column_from_square(self):
        f = Field(self.field_shifted)
        self.assertEqual(
            f.part_from("column", f.part("square", 0), 0),
            (f.part("column", 0), 0))
        self.assertEqual(
            f.part_from("column", f.part("square", 0), 6),
            (f.part("column", 0), 2))
        self.assertEqual(
            f.part_from("column", f.part("square", 0), 8),
            (f.part("column", 2), 2))
        self.assertEqual(
            f.part_from("column", f.part("square", 1), 0),
            (f.part("column", 3), 0))
        self.assertEqual(
            f.part_from("column", f.part("square", 3), 0),
            (f.part("column", 0), 3))
        self.assertEqual(
            f.part_from("column", f.part("square", 4), 4),
            (f.part("column", 4), 4))
        self.assertEqual(
            f.part_from("column", f.part("square", 1), 8),
            (f.part("column", 5), 2))
        self.assertEqual(
            f.part_from("column", f.part("square", 8), 8),
            (f.part("column", 8), 8))
        self.assertEqual(
            f.part_from("column", f.part("square", 5), 5),
            (f.part("column", 8), 4))
        self.assertEqual(
            f.part_from("column", f.part("square", 6), 6),
            (f.part("column", 0), 8))

    def test_square_from_row(self):
        f = Field(self.field_shifted)
        self.assertEqual(
            f.part_from("square", f.part("row", 0), 0),
            (f.part("square", 0), 0))
        self.assertEqual(
            f.part_from("square", f.part("row", 0), 2),
            (f.part("square", 0), 2))
        self.assertEqual(
            f.part_from("square", f.part("row", 0), 3),
            (f.part("square", 1), 0))
        self.assertEqual(
            f.part_from("square", f.part("row", 0), 5),
            (f.part("square", 1), 2))
        self.assertEqual(
            f.part_from("square", f.part("row", 1), 0),
            (f.part("square", 0), 3))
        self.assertEqual(
            f.part_from("square", f.part("row", 2), 0),
            (f.part("square", 0), 6))
        self.assertEqual(
            f.part_from("square", f.part("row", 3), 0),
            (f.part("square", 3), 0))
        self.assertEqual(
            f.part_from("square", f.part("row", 3), 2),
            (f.part("square", 3), 2))
        self.assertEqual(
            f.part_from("square", f.part("row", 3), 3),
            (f.part("square", 4), 0))
        self.assertEqual(
            f.part_from("square", f.part("row", 4), 4),
            (f.part("square", 4), 4))
        self.assertEqual(
            f.part_from("square", f.part("row", 6), 6),
            (f.part("square", 8), 0))

    def test_square_from_column(self):
        f = Field(self.field_shifted)
        self.assertEqual(
            f.part_from("square", f.part("column", 5), 0),
            (f.part("square", 1), 2))
        self.assertEqual(
            f.part_from("square", f.part("column", 6), 6),
            (f.part("square", 8), 0))

    def test_set_1_in_square_5_at_1(self):
        f = Field(self.field_empty)
        self.assertEqual(f.part("square", 5)[1], 0)
        f.set_at(f.part("square", 5), 1, 1)
        self.assertEqual(f.part("square", 5)[1], 1)

    def test_set_7_in_square_5_at_2(self):
        f = Field(self.field_empty)
        self.assertEqual(f.part("square", 5)[2], 0)
        f.set_at(f.part("square", 5), 2, 7)
        self.assertEqual(f.part("square", 5)[2], 7)

    def test_set_1_in_row_5_at_1(self):
        f = Field(self.field_empty)
        self.assertEqual(f.part("row", 5)[1], 0)
        f.set_at(f.part("row", 5), 1, 1)
        self.assertEqual(f.part("row", 5)[1], 1)

    def test_set_8_in_row_5_at_2(self):
        f = Field(self.field_empty)
        self.assertEqual(f.part("row", 5)[2], 0)
        f.set_at(f.part("row", 5), 2, 8)
        self.assertEqual(f.part("row", 5)[2], 8)

    def test_set_1_in_column_5_at_1(self):
        f = Field(self.field_empty)
        self.assertEqual(f.part("column", 5)[1], 0)
        f.set_at(f.part("column", 5), 1, 1)
        self.assertEqual(f.part("column", 5)[1], 1)

    def test_set_5_in_column_6_at_2(self):
        f = Field(self.field_empty)
        self.assertEqual(f.part("column", 6)[2], 0)
        f.set_at(f.part("column", 6), 2, 5)
        self.assertEqual(f.part("column", 6)[2], 5)

    def test_complement_partnames(self):
        column_complements = Part.complements("column")
        row_complements = Part.complements("row")
        square_complements = Part.complements("square")
        self.assertTrue("row" in column_complements)
        self.assertTrue("row" in square_complements)
        self.assertTrue("column" in row_complements)
        self.assertTrue("column" in square_complements)
        self.assertTrue("square" in row_complements)
        self.assertTrue("square" in column_complements)
        with self.assertRaises(ValueError):
            Part.complements("qwer")


class TestSudokuValidation(TestSudokuCommonData):
    def test_column_valudation_on_correct(self):
        self.assertTrue(Validation.are_columns_valid(Field(self.field_rows)))

    def test_column_valudation_on_wrong(self):
        self.assertFalse(
            Validation.are_columns_valid(Field(self.field_columns)))

    def test_row_valudation_on_correct(self):
        self.assertTrue(Validation.are_rows_valid(Field(self.field_columns)))

    def test_row_valudation_on_wrong(self):
        self.assertFalse(Validation.are_rows_valid(Field(self.field_rows)))

    def test_square_valudation_on_correct(self):
        self.assertTrue(
            Validation.are_squares_valid(
                Field(self.field_squares_correct)))

    def test_square_valudation_on_empty(self):
        self.assertTrue(
            Validation.are_squares_valid(
                Field([0 for x in range(9 * 9)])))

    def test_square_valudation_on_wrong(self):
        self.assertFalse(
            Validation.are_squares_valid(Field(self.field_squares)))

    def test_full_validation_on_correct(self):
        self.assertTrue(Validation.is_valid_sudoku(Field(self.field_correct)))

    def test_full_validation_on_correct_incomplete(self):
        self.assertTrue(Validation.is_valid_sudoku(Field(self.field_correct)))

    def test_full_validation_on_wrong(self):
        self.assertFalse(Validation.is_valid_sudoku(Field(self.field_shifted)))

    def test_full_completeness_on_correct(self):
        self.assertTrue(Validation.is_complete_sudoku(
            Field(self.field_correct)))

    def test_full_completeness_on_correct_incomplete(self):
        self.assertFalse(Validation.is_complete_sudoku(
            Field(self.field_correct_incomplete)))


class TestSudokuLogic(TestSudokuCommonData):
    def test_count_of_shows_exactly_one_of_any_number_on_correct_sudoku(self):
        col = Field(self.field_correct).part("column", 1)
        self.assertEqual(Logic.count_of(col, 5), 1)
        row = Field(self.field_correct).part("row", 4)
        self.assertEqual(Logic.count_of(row, 6), 1)
        square = Field(self.field_correct).part("square", 8)
        self.assertEqual(Logic.count_of(row, 3), 1)

    def test_count_of_throws_on_invalid_number_argument(self):
        with self.assertRaises(ValueError):
            Logic.count_of([1, 2, 3, 4, 5, 6, 7, 8, 9], 0)
        with self.assertRaises(ValueError):
            Logic.count_of([1, 2, 3, 4, 5, 6, 7, 8, 9], 10)
        with self.assertRaises(ValueError):
            Logic.count_of([1, 2, 3, 4, 5, 6, 7, 8, 9], 21)

    def test_count_of_1_in_list_of_nine_ones_is_9(self):
        self.assertEqual(Logic.count_of([1, 1, 1, 1, 1, 1, 1, 1, 1], 1), 9)

    def test_count_of_1_in_list_single_1_is_1(self):
        self.assertEqual(Logic.count_of([0, 0, 0, 0, 0, 0, 0, 0, 1], 1), 1)
        self.assertEqual(Logic.count_of([0, 0, 0, 0, 1, 0, 0, 0, 0], 1), 1)
        self.assertEqual(Logic.count_of([0, 0, 0, 0, 0, 0, 0, 1, 0], 1), 1)

    def test_count_of_ignores_zeros_in_list(self):
        self.assertEqual(Logic.count_of([0, 1, 1, 1, 1, 1, 1, 1, 1], 1), 8)
        self.assertEqual(Logic.count_of([0, 0, 0, 0, 0, 0, 0, 0, 9], 9), 1)

    def test_count_of_empty_in_list_of_zeros_is_9(self):
        self.assertEqual(Logic.count_of_empty([0, 0, 0, 0, 0, 0, 0, 0, 0]), 9)

    def test_count_of_empty_in_list_of_ones_is_0(self):
        self.assertEqual(Logic.count_of_empty([1, 1, 1, 1, 1, 1, 1, 1, 1]), 0)

    def test_places_in_first_square_for_1_based_on_rows_and_columns(self):
        f = Field(self.field_solve_square_1)
        self.assertEqual(Logic.places_in_part(f, f.part("square", 0), 1), [8])

    def test_places_in_first_square_for_4_based_on_single_empty_cell(self):
        f = Field(self.field_solve_sqare_2)
        self.assertEqual(Logic.places_in_part(f, f.part("square", 0), 4), [5])

    def test_places_in_seventh_square_for_2_based_on_both_rules(self):
        f = Field(self.field_solve_sqare_3)
        self.assertEqual(Logic.places_in_part(f, f.part("square", 7), 2), [3])

    def test_places_in_first_row_for_3_based_on_squares_and_columns(self):
        f = Field(self.field_solve_row_1)
        self.assertEqual(Logic.places_in_part(f, f.part("row", 0), 3), [8])

    def test_places_in_first_row_for_6_based_on_single_empty_cell(self):
        f = Field(self.field_solve_row_2)
        self.assertEqual(Logic.places_in_part(f, f.part("row", 0), 6), [1])

    def test_places_in_third_column_for_9_based_on_both_rules(self):
        f = Field(self.field_solve_column_1)
        self.assertEqual(Logic.places_in_part(f, f.part("column", 3), 9), [6])


def solve_part(field, part):
    for i in range(1, len(part) + 1):
        places = Logic.places_in_part(field, part, i)
        if len(places) == 1:
            field.set_at(part, places[0], i)
            part = field.part(part.type(), part.index())


def solve(field):
    before = str(field)
    while True:
        for parttype in Part.types():
            for part in f.parts(parttype):
                solve_part(field, part)
        after = str(field)
        if after == before:
            break
        before = after


if __name__ == '__main__':
    f = Field(json.load(sys.stdin))
    print("before:\n{}".format(f))
    solve(f)
    print("after:\n{}".format(f))

    if not Validation.is_valid_sudoku(f):
        print('wrong')
    if not Validation.is_complete_sudoku(f):
        print('incomplete')
    else:
        print('ok')
